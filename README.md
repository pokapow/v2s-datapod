# v2s-datapod

Hasura stack with Deno middleware to store offchain data alongside Duniter v2s.

You will need Docker for both dev/prod mode.

## Launch in dev mode

- Create `.env` from `.env.example` and customize it
- Install Deno: `curl -fsSL https://deno.land/x/install/install.sh | sh`
- Then:

```bash
./load.sh dev
```

- Go to <http://localhost:8080> to use Hasura console

## Launch in production mode

```bash
./load.sh
```
