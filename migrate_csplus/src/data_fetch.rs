use bb8::Pool;
use bb8_postgres::PostgresConnectionManager;
use reqwest;
use std::{env, error::Error, sync::Arc};
use tokio::sync::Mutex;
use tokio_postgres::NoTls;

use crate::{
    db::insert_profiles,
    fetch_addresses_from_squid::fetch_ids_from_graphql,
    models::{Hit, ScrollResponse},
};

/// Fetches and processes profiles in batches, overlapping fetching next batch during processing.
pub async fn fetch_profiles(
    pool_arc: Arc<Mutex<Pool<PostgresConnectionManager<NoTls>>>>,
) -> Result<(), Box<dyn Error>> {
    let page_size = 5000;
    let mut iteration = 1;

    // Prepare requests and body values
    let csplus_endpoint = env::var("CSPLUS_ENDPOINT").map_err(|_| "CSPLUS_ENDPOINT must be set")?;
    let url_profiles = format!("{csplus_endpoint}/user/profile/_search?scroll=5m");
    let scroll_batch_url = format!("{}/_search/scroll", csplus_endpoint);
    let initial_batch_body = serde_json::json!({ "query": { "match_all": {} }, "size": page_size });

    // Make an initial request to get the total number of profiles
    let total_profiles = fetch_total_profiles_count(&url_profiles).await?;
    let total_pages = (total_profiles as f64 / page_size as f64).ceil() as usize;

    println!(
        "Start migration of {} CS+ profiles from {} endpoint ...",
        total_profiles, csplus_endpoint
    );
    println!("Retrieving page: {}/{}", iteration, total_pages);

    // Fetch initial batch
    let mut future_scroll_response =
        fetch_batch(url_profiles.to_string(), initial_batch_body).await;

    // Fetch IDs from the GraphQL endpoint
    let ids = Arc::new(fetch_ids_from_graphql().await?);

    while let Ok(current_response) = future_scroll_response {
        // Process the current batch
        process_hits(pool_arc.clone(), &current_response.hits.hits, &ids).await?;

        // Log the retrieval or completion
        iteration += 1;
        if iteration <= total_pages && !current_response.hits.hits.is_empty() {
            println!("Retrieving page: {}/{}", iteration, total_pages);
        } else {
            println!("CS+ data are fully imported!");
            break;
        }

        // Prepare for the next iteration
        let scroll_batch_body =
            serde_json::json!({ "scroll": "5m", "scroll_id": &current_response.scroll_id });
        future_scroll_response = fetch_batch(scroll_batch_url.clone(), scroll_batch_body).await;
    }

    Ok(())
}

/// Fetches the batch of data.
async fn fetch_batch(
    endpoint_url: String,
    body: serde_json::Value,
) -> Result<ScrollResponse, Box<dyn Error>> {
    let client = reqwest::Client::new();
    let response = client
        .post(endpoint_url)
        .json(&body)
        .send()
        .await?
        .json::<ScrollResponse>()
        .await?;

    Ok(response)
}

/// Processes the hits from the fetched data.
async fn process_hits(
    pool: Arc<Mutex<Pool<PostgresConnectionManager<NoTls>>>>,
    hits: &[Hit],
    ids: &Arc<Vec<String>>,
) -> Result<(), Box<dyn Error>> {
    println!("Processing...");
    let mut tasks = Vec::new();

    for chunk in hits.chunks(2000) {
        let pool_clone = pool.clone();
        let chunk_clone = chunk.to_vec();
        let ids_clone = Arc::clone(&ids);

        let task = tokio::spawn(async move {
            if let Ok(mut client) = pool_clone.lock().await.get().await {
                if let Err(e) = insert_profiles(&mut client, &chunk_clone, ids_clone.as_ref()).await
                {
                    eprintln!("Insert error: {}", e);
                }
            }
        });

        tasks.push(task);
    }

    // Await all processing tasks to complete
    for task in tasks {
        task.await?;
    }

    Ok(())
}

async fn fetch_total_profiles_count(url: &str) -> Result<usize, Box<dyn Error>> {
    let body = serde_json::json!({ "query": { "match_all": {} }, "size": 0 });

    match fetch_batch(url.to_string(), body).await {
        Ok(response) => {
            let total_hits = response.hits.total;
            Ok(total_hits as usize)
        }
        Err(e) => Err(e),
    }
}
