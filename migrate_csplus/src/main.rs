mod data_fetch;
mod db;
mod models;
mod utils;
mod fetch_addresses_from_squid;

use crate::db::database_connection;

#[tokio::main]
async fn main() {
    // Initialize database connection
    match database_connection().await {
        Ok(db_pool_arc) => {
            // Start the data fetching process
            if let Err(e) = data_fetch::fetch_profiles(db_pool_arc).await {
                eprintln!("Error during profile fetching: {}", e);
            }
        }
        Err(e) => eprintln!("Failed to establish database connection: {}", e),
    }
}
