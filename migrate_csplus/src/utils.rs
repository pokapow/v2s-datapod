use base64::{engine::general_purpose, DecodeError, Engine as _};
use bs58;
use chrono::{DateTime, TimeZone, Utc};
use serde_json::Value;
use sp_core::crypto::{AccountId32, Ss58Codec};
use std::convert::TryFrom;

/// Converts a geolocation value (if present) to a tuple of (latitude, longitude).
pub fn convert_geoloc(geoloc_value: Option<&Value>) -> Option<(f64, f64)> {
    geoloc_value.and_then(|v| v.as_object()).and_then(|geoloc| {
        let lat = geoloc
            .get("lat")
            .and_then(Value::as_f64)
            .unwrap_or_default();
        let lon = geoloc
            .get("lon")
            .and_then(Value::as_f64)
            .unwrap_or_default();
        Some((lat, lon))
    })
}

/// Decodes a Base64-encoded avatar image into a byte vector.
pub fn decode_avatar(base64_str: &str) -> Result<Vec<u8>, DecodeError> {
    let mut buf = Vec::new();
    general_purpose::STANDARD.decode_vec(base64_str.as_bytes(), &mut buf)?;
    Ok(buf)
}

/// Converts a Base58-encoded string to an SS58-encoded string.
pub fn base58_to_ss58(base58: &str) -> Result<String, Box<dyn std::error::Error>> {
    let pubkey_bytes = bs58::decode(base58).into_vec()?;
    let pubkey_array = <[u8; 32]>::try_from(pubkey_bytes.as_slice())?;
    let account_id = AccountId32::from(pubkey_array);
    Ok(account_id.to_ss58check())
}

/// Replace empty string by null value
pub fn empty_str_to_none(value: Option<&str>) -> Option<String> {
    match value {
        Some(s) if !s.is_empty() => Some(s.to_string()),
        _ => None,
    }
}

pub fn convert_timestamp(timestamp: i64) -> DateTime<Utc> {
    Utc.timestamp_opt(timestamp, 0).unwrap()
}
