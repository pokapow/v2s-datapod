use crate::utils::*;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{collections::HashMap, error::Error};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Hit {
    #[serde(rename = "_source")]
    pub source: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ScrollResponse {
    #[serde(rename = "_scroll_id")]
    pub scroll_id: String,
    pub hits: Hits,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Hits {
    pub hits: Vec<Hit>,
    pub total: u64,
}

pub struct ProfileRow {
    pub address: Option<String>,
    pub avatar: Option<Vec<u8>>,
    pub description: Option<String>,
    pub geoloc: Option<(f64, f64)>,
    pub title: Option<String>,
    pub city: Option<String>,
    pub socials: Option<Value>,
    pub created_at: Option<DateTime<Utc>>,
}

impl ProfileRow {
    pub fn from_hit(hit: &Hit) -> Result<Self, Box<dyn Error>> {
        let profile = &hit.source;
        let timestamp = profile.get("time").and_then(Value::as_i64);
        let created_at = timestamp.map(convert_timestamp);

        Ok(Self {
            address: empty_str_to_none(profile.get("issuer").and_then(Value::as_str)).map(|s| {
                base58_to_ss58(&s).expect(&format!("Cannot convert pubkey {} to address", s))
            }),
            avatar: profile
                .get("avatar")
                .and_then(|av| av.get("_content"))
                .and_then(Value::as_str)
                .and_then(|base64| decode_avatar(base64).ok()),
            description: empty_str_to_none(profile.get("description").and_then(Value::as_str)),
            geoloc: convert_geoloc(profile.get("geoPoint")),
            title: empty_str_to_none(profile.get("title").and_then(Value::as_str)),
            city: empty_str_to_none(profile.get("city").and_then(Value::as_str)),
            socials: profile
                .get("socials")
                .map(|s| serde_json::to_value(s).unwrap_or(serde_json::Value::Null)),
            created_at,
        })
    }
}
