use crate::models::{Hit, ProfileRow};
use bb8::Pool;
use bb8_postgres::PostgresConnectionManager;
use dotenv::dotenv;
use std::{env, error::Error, sync::Arc};
use tokio::sync::Mutex;
use tokio_postgres::{types::ToSql, Client, NoTls};

/// Create a pool connection to Postgres database
pub async fn database_connection(
) -> Result<Arc<Mutex<Pool<PostgresConnectionManager<NoTls>>>>, Box<dyn Error>> {
    let is_production = env::var("PRODUCTION").unwrap_or_else(|_| String::from("false")) == "true";
    if !is_production {
        match dotenv() {
            Ok(_) => {}
            Err(err) => println!("Error loading .env file: {}", err),
        }
    }

    let db_user = env::var("DB_USER").map_err(|_| "DB_USER must be set")?;
    let db_password = env::var("DB_PASSWORD").map_err(|_| "DB_PASSWORD must be set")?;
    let db_database = env::var("DB_DATABASE").map_err(|_| "DB_DATABASE must be set")?;
    let db_hostname = if is_production {
        "postgres-datapod"
    } else {
        "localhost"
    };
    let database_url = format!(
        "postgres://{}:{}@{}:5432/{}",
        db_user, db_password, db_hostname, db_database
    );

    // Database connection setup
    let manager = PostgresConnectionManager::new_from_stringlike(database_url, NoTls)
        .map_err(|e| Box::new(e) as Box<dyn Error>)?;
    let pool = Pool::builder()
        .build(manager)
        .await
        .map_err(|e| Box::new(e) as Box<dyn Error>)?;

    Ok(Arc::new(Mutex::new(pool)))
}

/// Insert parsed profiles to Postgres database
pub async fn insert_profiles(
    client: &mut Client,
    hits: &[Hit],
    ids: &[String],
) -> Result<(), Box<dyn Error>> {
    let transaction = client.transaction().await?;

    for hit in hits {
        let profile_row = ProfileRow::from_hit(hit)?;

        // Skip insert if address is not in the list of IDs
        if profile_row
            .address
            .as_ref()
            .map_or(true, |address| !ids.contains(address))
        {
            continue;
        }

        // Construct the query using optional parameters for nullable fields
        let statement = transaction.prepare(
            "INSERT INTO public.profiles (address, avatar, description, geoloc, title, city, socials, created_at)
            VALUES ($1, $2, $3, point($4, $5), $6, $7, $8, $9)"
        ).await?;

        // Prepare parameters, handling None values appropriately
        let params: [&(dyn ToSql + Sync); 9] = [
            &profile_row.address,
            &profile_row.avatar,
            &profile_row.description,
            &profile_row.geoloc.map(|(lat, _)| lat),
            &profile_row.geoloc.map(|(_, lon)| lon),
            &profile_row.title,
            &profile_row.city,
            &profile_row.socials,
            &profile_row.created_at,
        ];

        transaction.execute(&statement, &params).await?;
    }

    transaction.commit().await?;
    Ok(())
}
