use reqwest;
use serde::Deserialize;
use serde_json::json;
use std::{env, error::Error};

// Define a structure to deserialize the GraphQL response
#[derive(Deserialize)]
struct GraphQLResponse {
    data: GraphQLData,
}

#[derive(Deserialize)]
struct GraphQLData {
    accounts: Vec<Account>,
}

#[derive(Deserialize)]
struct Account {
    id: String,
}

/// Get all existing accounts on squid indexer
pub async fn fetch_ids_from_graphql() -> Result<Vec<String>, Box<dyn Error>> {
    let endpoint = env::var("SQUID_ENDPOINT").map_err(|_| "SQUID_ENDPOINT must be set")?;
    println!("Fetch addresses from squid endpoint {} ...", endpoint);

    // Define the GraphQL query
    let graphql_query = json!({
        "query": "{ accounts { id } }"
    });

    // Send the request
    let client = reqwest::Client::new();
    let res = client.post(endpoint).json(&graphql_query).send().await?;

    // Check if the request was successful
    if res.status().is_success() {
        // Parse the response
        let response_body: GraphQLResponse = res.json().await?;
        // Extract the IDs
        let ids = response_body
            .data
            .accounts
            .into_iter()
            .map(|account| account.id)
            .collect();
        Ok(ids)
    } else {
        Err(Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Failed to fetch data from GraphQL endpoint",
        )))
    }
}
