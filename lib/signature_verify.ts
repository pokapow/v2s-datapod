import {
  base64Decode,
  signatureVerify,
} from "https://deno.land/x/polkadot@0.2.45/util-crypto/mod.ts";
import { Profile, Transaction } from "./types.ts";

export enum SignatureResponse {
  valid,
  invalidHash,
  invalidSignature,
  requiredFieldsMissed,
}

export const signatureResponseMessages: { [key in SignatureResponse]: string } =
  {
    [SignatureResponse.valid]: "The signature is valid.",
    [SignatureResponse.invalidHash]: "The hash is invalid.",
    [SignatureResponse.invalidSignature]: "The signature is invalid.",
    [SignatureResponse.requiredFieldsMissed]:
      "Required fields address, hash or signature is missed.",
  };

export async function verifySignature(
  profile: Profile | Transaction,
): Promise<SignatureResponse> {
  let payload: string;
  let addressSign: string;

  // Validate input
  if (
    !profile.address || !profile.hash ||
    !profile.signature
  ) {
    return SignatureResponse.requiredFieldsMissed;
  }

  // If oldAddress is present, then we are on a profile migration event
  if ("oldAddress" in profile) {
    const { oldAddress, address } = profile;
    payload = JSON.stringify({
      oldAddress,
      address,
    });
    addressSign = oldAddress!;
  // If comment is present, then we are on a add transaction event
  } else if ("comment" in profile) {
    const { address, id, comment } = profile;
    payload = JSON.stringify({
      id,
      address,
      comment,
    });
    addressSign = address!;
    // Else we are on an update or delete profilie event
  } else {
    const { address, description, avatarBase64, geoloc, title, city, socials } =
      profile;
    payload = JSON.stringify({
      address,
      description,
      avatarBase64,
      geoloc,
      title,
      city,
      socials,
    });
    addressSign = address;
  }

  try {
    const hashVerify = await createHashedMessage(payload);
    if (profile.hash != hashVerify) {
      console.error("hash documents is invalid");
      return SignatureResponse.invalidHash;
    }

    const messageUint8Array = new TextEncoder().encode(profile.hash);
    const signature = base64Decode(profile.signature);
    const signedMessage = signatureVerify(
      messageUint8Array,
      signature,
      addressSign,
    );

    return signedMessage.isValid
      ? SignatureResponse.valid
      : SignatureResponse.invalidSignature;
  } catch (error) {
    console.error("Signature verification failed:", error);
    throw new Error(`Cannot verify signature`);
  }
}

async function createHashedMessage(messageToSign: string): Promise<string> {
  // Convert to bytes
  const encoder = new TextEncoder();
  const data = encoder.encode(messageToSign);

  // Calculate SHA-256
  const hashBuffer = await crypto.subtle.digest("SHA-256", data);
  const hashArray = Array.from(new Uint8Array(hashBuffer));

  // Convert to hexa
  const hashHex = hashArray.map((byte) => byte.toString(16).padStart(2, "0"))
    .join("").toUpperCase();

  return hashHex;
}
