import {
  ApiPromise,
  WsProvider,
} from "https://deno.land/x/polkadot@0.2.45/api/mod.ts";

class ApiDuniter {
  private static instance: ApiPromise | null = null;

  private constructor() {}

  /**
   * Creates or returns the existing ApiPromise instance.
   * Can throw an error if the ApiPromise creation fails.
   * @returns {Promise<ApiPromise>}
   */
  public static async getInstance(endpoints?: string[]): Promise<ApiPromise> {
    if (!ApiDuniter.instance) {
      try {
        const provider = new WsProvider(endpoints);
        ApiDuniter.instance = await ApiPromise.create({ provider });
      } catch (error) {
        console.error("Failed to create ApiPromise instance:", error);
        throw error;
      }
    }
    return ApiDuniter.instance;
  }

  /**
   * Disconnects the ApiPromise instance and cleans up resources
   */
  public static async disconnect(): Promise<void> {
    if (ApiDuniter.instance) {
      await ApiDuniter.instance.disconnect();
      ApiDuniter.instance = null;
    }
  }
}

export default ApiDuniter;
