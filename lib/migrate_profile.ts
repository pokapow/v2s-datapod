import { Context } from "https://deno.land/x/oak@v12.6.1/context.ts";
import { Client } from "https://deno.land/x/postgres@v0.17.0/client.ts";
import { Profile } from "./types.ts";
import {
  SignatureResponse,
  signatureResponseMessages,
  verifySignature,
} from "./signature_verify.ts";
import { checkRecordExist } from "./utils.ts";

export async function migrateProfile(ctx: Context, client: Client) {
  try {
    const body = await ctx.request.body().value;
    const profile: Profile = body.variables || body.input || {};

    // Validate input
    if (!profile.oldAddress) {
      ctx.response.status = 400;
      ctx.response.body = {
        success: false,
        message: "oldAddress is required.",
      };
      return;
    }

    // Verify signature
    const signatureResult = await verifySignature(profile);
    if (signatureResult !== SignatureResponse.valid) {
      ctx.response.status = 400;
      console.error(
        "Invalid signature: " + signatureResponseMessages[signatureResult],
      );
      ctx.response.body = {
        success: false,
        message: "Invalid signature: " +
          signatureResponseMessages[signatureResult],
      };
      return;
    }

    // Verify 2 addresses are not the same
    if (profile.oldAddress === profile.address) {
      ctx.response.status = 422;
      console.error(
        `Profile ${profile.oldAddress} and ${profile.address} are the same.`,
      );
      ctx.response.body = {
        success: false,
        message:
          `Profile ${profile.oldAddress} and ${profile.address} are the same.`,
      };
      return;
    }

    // Verify old profile exists
    if (!await checkRecordExist(client, 'profiles', 'address', profile.oldAddress)) {
      ctx.response.status = 404;
      console.error(`Profile ${profile.oldAddress} does not exist.`);
      ctx.response.body = {
        success: false,
        message: `Profile ${profile.oldAddress} does not exist.`,
      };
      return;
    }

    // Verify new profile doesn't exists
    if (await checkRecordExist(client, 'profiles', 'address', profile.address)) {
      ctx.response.status = 422;
      console.error(`Profile ${profile.address} already exist.`);
      ctx.response.body = {
        success: false,
        message: `Profile ${profile.address} already exist.`,
      };
      return;
    }

    // Prepare and execute database query for deletion
    const migrateQuery = `
    UPDATE public.profiles
    SET address = $1
    WHERE address = $2;
    `;

    try {
      await client.queryObject({
        text: migrateQuery,
        args: [profile.address, profile.oldAddress],
      });
      ctx.response.status = 200;
      console.log(
        `Profile ${profile.oldAddress} has been migrate to ${profile.address} profile.`,
      );
      ctx.response.body = {
        success: true,
        message:
          `Profile ${profile.oldAddress} has been migrate to ${profile.address} profile.`,
      };
    } catch (error) {
      console.error("Database error in migrating profile:", error);
      ctx.response.status = 500;
      ctx.response.body = {
        success: false,
        message: "Internal server error: " + error,
      };
    }
  } catch (error) {
    console.error("Error migrate profile:", error);
    ctx.response.status = 500;
    ctx.response.body = {
      success: false,
      message: "Error migrate profile: " + error,
    };
  }
}
