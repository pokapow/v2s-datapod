#!/bin/bash

option=$1

if [[ $option == "dev" ]]; then
    echo "Start datapod in dev mode"
    docker compose -f docker-compose.yml -f docker-compose.override.yml down -v
    docker compose up -d
    deno run --allow-env --allow-read --allow-write --allow-net --allow-run --watch index.ts
else
    echo "Start datapod in production mode"
    docker compose -f docker-compose.yml -f docker-compose.prod.yml down
    docker compose -f docker-compose.yml -f docker-compose.prod.yml up -d
    [[ $option == "log" ]] && docker compose -f docker-compose.yml -f docker-compose.prod.yml logs -n200 -f
fi
