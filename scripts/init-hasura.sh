#!/bin/sh

# start hasura in background
graphql-engine serve &

endpoint="http://localhost:8080"

check_hasura_ready() {
  response=$(curl -s -o /dev/null -w "%{http_code}" $endpoint/healthz)
  echo $response
}

echo "Waiting for hasura..."
while [ $(check_hasura_ready) -ne 200 ]; do
  sleep 1
done

echo "Hasura is ready."
if [ "$PRODUCTION" = "true" ]; then
  sed -i 's/host.docker.internal:3000/datapod-app:3000/g' hasura/metadata/actions.yaml
else
  sed -i 's/datapod-app:3000/host.docker.internal:3000/g' hasura/metadata/actions.yaml
fi

hasura migrate apply --endpoint $endpoint --admin-secret $HASURA_GRAPHQL_ADMIN_SECRET --database-name default
hasura metadata apply --endpoint $endpoint --admin-secret $HASURA_GRAPHQL_ADMIN_SECRET

while true; do
  sleep 360
done
