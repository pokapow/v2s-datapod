#!/bin/bash

export $(cat .env | grep -E 'HASURA_GRAPHQL_ADMIN_SECRET|HASURA_LISTEN_PORT')
endpoint="http://localhost:$HASURA_LISTEN_PORT"

hasura metadata export --endpoint $endpoint --admin-secret $HASURA_GRAPHQL_ADMIN_SECRET

# To manually apply saved metadata:
# hasura metadata apply --endpoint $endpoint --admin-secret $HASURA_GRAPHQL_ADMIN_SECRET
