#!/bin/bash

set -e

tag=$1
[[ ! $tag ]] && tag="latest"

docker build -t duniter-datapod-app .
docker image tag duniter-datapod-app:$tag poka/duniter-datapod-app
docker image push poka/duniter-datapod-app:$tag
